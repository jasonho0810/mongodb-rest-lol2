package com.training.mongodbrestlol.service;

import com.training.mongodbrestlol.model.LolChampion;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class LolServiceIntegrationTest {

    @Autowired
    private LolService lolService;

    @Test // Read
    public void testLolServiceFindAll() {
        List<LolChampion> allChampions = lolService.findAll();

        assertEquals(0, allChampions.size());
        // System.out.println(allChampions.get(0));
    }

}
