package com.training.mongodbrestlol.service;

import com.training.mongodbrestlol.model.LolChampion;
import com.training.mongodbrestlol.repository.LolRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest // look for main configuration class
public class LolServiceTest {

    @Autowired
    private LolService lolService;

    @MockBean // bean object without dependencies, for unit testing
    private LolRepository lolRepository;

    @Test // Read
    public void testLolServiceFindAll() {
        List<LolChampion> allChampions = new ArrayList<LolChampion>();
        LolChampion testLol = new LolChampion();
        testLol.setId("12345");
        allChampions.add(testLol);

        when(lolRepository.findAll()).thenReturn(allChampions);

        assertEquals(1, lolService.findAll().size());
    }

}
