package com.training.mongodbrestlol.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LolChampionTest {

    @BeforeEach // setup work that needs to happen before tests run
    public void runBeforeEachTest() { System.out.println("this runs first!!"); }

    @Test // getters/setters
    public void testLolGettersSetters() {
        LolChampion testLol = new LolChampion();
        testLol.setId("12345");
        testLol.setName("Abc");
        testLol.setRole("Def");
        testLol.setDifficulty("6");

        assertEquals("12345", testLol.getId());
        assertEquals("Abc", testLol.getName());
        assertEquals("Def", testLol.getRole());
        assertEquals("6", testLol.getDifficulty());
    }

}
