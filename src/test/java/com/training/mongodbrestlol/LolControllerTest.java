package com.training.mongodbrestlol;

import com.training.mongodbrestlol.model.LolChampion;
import com.training.mongodbrestlol.rest.LolController;
import com.training.mongodbrestlol.service.LolServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// test only Spring MVC components (Component, Service or Repository beans)
@WebMvcTest(LolController.class)
public class LolControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LolServiceInterface lolService;

    @Test
    public void testLolControllerFindAll() throws Exception {
        List<LolChampion> allChampions = new ArrayList<LolChampion>();
        LolChampion testLol = new LolChampion();
        testLol.setId("12345");
        allChampions.add(testLol);

        when(lolService.findAll()).thenReturn(allChampions);

        this.mockMvc.perform(get("/api/v1/lol"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"12345\"")));
    }

}
