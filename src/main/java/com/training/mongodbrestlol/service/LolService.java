package com.training.mongodbrestlol.service;

import com.training.mongodbrestlol.model.LolChampion;
import com.training.mongodbrestlol.repository.LolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component // service bean object
public class LolService implements LolServiceInterface {

    @Autowired // autowire to repository bean object
    private LolRepository lolRepository;

    // Read
    @Override // override interface implementation
    public List<LolChampion> findAll() {
        return lolRepository.findAll();
    }

    // Optional returns null if cannot find
    @Override
    public Optional<LolChampion> findById(String id) {
        return lolRepository.findById(id);
    }

    // Create/Update
    @Override
    public LolChampion save(LolChampion lolChampion) {
        return lolRepository.save(lolChampion);
    }

    // Delete
    @Override
    public void delete(String id) {
        lolRepository.deleteById(id);
    }

}
