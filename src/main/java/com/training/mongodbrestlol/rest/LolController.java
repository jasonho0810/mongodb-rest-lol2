package com.training.mongodbrestlol.rest;

import com.training.mongodbrestlol.model.LolChampion;
import com.training.mongodbrestlol.service.LolServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController // Component + receive HTTP Request from spring libraries
@RequestMapping("/api/v1/lol") // specify endpoint url
public class LolController {

    private static final Logger LOG = LoggerFactory.getLogger(LolController.class);

    @Autowired
    private LolServiceInterface lolService;

    @GetMapping
    public List<LolChampion> findAll() {
        return lolService.findAll();
    }

    // GET HTTP Request
    @GetMapping("{id}") // GET by id
    // PathVariable to handle template variables in RequestMapping
    public ResponseEntity<LolChampion> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<LolChampion>(lolService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        return lolService.save(lolChampion);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(lolService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        lolService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
