package com.training.mongodbrestlol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // instantiate container, scan directory structure for components, add components into container (bean)
public class MongodbRestLolApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongodbRestLolApplication.class, args);
    }

}
